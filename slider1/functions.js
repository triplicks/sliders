class slider{
	constructor(slider){
		this.slider = slider;
		this.substrate = slider.querySelector('#slider_wrap');
		this.slides = slider.querySelectorAll('.slider_item');
		this.slideLength = slider.querySelectorAll('.slider_item').length;
		this.navigation = document.querySelector('.navigation.'+this.slider.getAttribute('id'));
		this.generationSlidesFromSlider();
		this.navigation.querySelector('.next').onclick = e=>{this.next()};
		this.navigation.querySelector('.prev').onclick = e=>{this.prev()};
	}
	generationSlidesFromSlider(){
		for (let i = 0; i < this.slideLength; i++) {this.slides[i].style.width = this.slider.clientWidth+"px";}
		this.substrate.style.width = this.slideLength * this.slider.clientWidth +50 +"px";
		this.slides[0].classList.add('activeSlide');
		this.slides[this.slides.length-1].classList.add('endSlide');
	}
	remove(){
		this.remove();
	}
	next(){
		let self = this,
			slide = this.slider.querySelector('.activeSlide'),
			slides = this.slider.querySelectorAll('.slider_item'),
			keyNext = [].indexOf.call(slides,slide)+1;
		this.slider.querySelector('.endSlide').classList.remove('endSlide');
		slide.classList.remove('activeSlide');
		(keyNext!=slides.length)?slides[keyNext].classList.add('activeSlide'):slides[0].classList.add('activeSlide');
		let clone = slide.cloneNode(true);
		clone.style.width = self.slider.clientWidth+"px";
		clone.classList.add('endSlide');
		self.substrate.appendChild(clone);
		slide.addEventListener('transitionend',self.remove);
		slide.style.width = 0;
		
	}
	prev(){
		let self = this,
			slide = this.slider.querySelector('.endSlide'),
			slides = this.slider.querySelectorAll('.slider_item'),
			keyPrev = [].indexOf.call(slides,slide)-1,
			activeSlide = this.slider.querySelector('.activeSlide');
		slide.classList.remove('endSlide');
		activeSlide.classList.remove('activeSlide');
		slides[keyPrev].classList.add('endSlide');
		let clone = slide.cloneNode(true);
		clone.classList.add('activeSlide');
		clone.style.width = 0;
		this.substrate.insertBefore(clone,activeSlide);
		clone.style.width = this.slider.clientWidth + "px";
		slide.remove();

	}
}
export {slider};