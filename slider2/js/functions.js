class slider{
	constructor(slider){
		this.slider = slider;
		this.substrate = slider.querySelector('.substrate');
		let self = this;
		for (var i = 0; i < slider.querySelectorAll('.slide').length; i++) {
			slider.querySelectorAll('.slide')[i].onclick = e=>{self.clickSlide(e.currentTarget)};
		}
		this.generation();
	}
	generation(){
		let activeSlide = this.slider.querySelector('.slide.active');
		let slides = this.slider.querySelectorAll('.slide');
		let activeKey = Math.round(slides.length/2)-1;
		this.substrate.style.width = slides.length * slides[0].clientWidth + 200 +"px";
		for (var i = 0; i < slides.length; i++) {
			if(i>activeKey-3&&i<activeKey+3){
				if(i<activeKey) slides[i].classList.add('left'); else
				if(i==activeKey) slides[i].classList.add('active'); else
				if(i>activeKey) slides[i].classList.add('right');	
				if(i<=activeKey) slides[i].style.right = '100px';
				if(i==activeKey-2) slides[i].style.right = '0px';
				if(i==activeKey+1) slides[i].style.right = '100px';
				if(i==activeKey+1) slides[i].style.zIndex = '2';
				if(i==activeKey+2) slides[i].style.right = '150px';
			}else{
				slides[i].classList.add('none');
			}
		}
	}
	remove(self){
		console.log(self);
		self.classList.add('none');
		self.removeAttribute('style');
		self.removeEventListener('transitionEnd',this.remove);		
	}
	refresh(){
		let activeSlide = this.slider.querySelector('.slide.active');
		let slides = this.slider.querySelectorAll('.slide');
		let activeKey = [].indexOf.call(slides,activeSlide);
		for (var i = 0; i < slides.length; i++) {
			slides[i].className = 'slide';
			slides[i].removeAttribute('style');
			if(i>activeKey-3&&i<activeKey+3){
				if(i<activeKey) slides[i].classList.add('left'); else
				if(i==activeKey) slides[i].classList.add('active'); else
				if(i>activeKey) slides[i].classList.add('right');
				if(i<=activeKey) slides[i].style.right = '100px';
				if(i==activeKey-2) slides[i].style.right = '0px';
				if(i==activeKey+1) slides[i].style.right = '100px';
				if(i==activeKey+1) slides[i].style.zIndex = '2';
				if(i==activeKey+2) slides[i].style.right = '150px';
			}else{
				slides[i].classList.add('none');
				slides[i].removeAttribute('style');
			}
		}	
	}
	clickSlide(self){
		if(!self.classList.contains('active')){
			this.slider.querySelector('.slide.active').classList.remove('active');
			self.classList.add('active');
			this.refresh();
		}else if(!self.classList.contains('activePopup')){
			let clone = self.cloneNode(true);
			let wrapper = document.createElement('div');
			wrapper.classList.add('wrapper');
			wrapper.style.display = 'block';
			wrapper.addEventListener('click', e=>{this.close()},true);
			document.querySelector('body').insertBefore(wrapper,document.querySelector('div'));
			clone.classList.add('empty');
			this.substrate.insertBefore(clone,this.slider.querySelector('.slide.active'));
			self.classList.add('activePopup');
			setTimeout(()=>{document.querySelector('.wrapper').style.opacity = 1;},100);
		}
	}
	close(){
		let self = document.querySelector('.activePopup');
		this.substrate.querySelector('.empty').remove();
		self.classList.remove('activePopup');
		document.querySelector('.wrapper').style.opacity = 0;
		setTimeout(()=>{document.querySelector('.wrapper').remove();},700);
	}
}

export {slider};